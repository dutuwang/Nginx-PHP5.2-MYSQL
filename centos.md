```
yum install gcc g++ make cmake gcc+ gcc-c++ libxml2 libxml2-devel openssl openssl-devel bzip2 bzip2-devel libcurl libcurl-devel libjpeg libjpeg-devel libpng libpng-devel freetype freetype-devel gmp gmp-devel libmcrypt libmcrypt-devel readline readline-devel libxslt libxslt-devel curl-devel libXpm-devel
```

```
#tar -xzvf pcre-8.00.tar.gz
#cd pcre-8.00
#./configure --prefix=/usr/local/pcre-8.00
#make && make install
#make clean
#cd ..
```

```
#tar -xzvf nginx-1.12.1.tar.gz
#cd nginx-1.12.1
#./configure --prefix=/usr/local/nginx --with-http_ssl_module --with-pcre=../pcre-8.00
#make
#make install
#make clean
#cd ..
```

//PHP安装
```
./configure --prefix=/usr/local/php \
 --with-config-file-path=/usr/local/php/etc \
 --with-curl \
 --with-freetype-dir \
 --with-gd \
 --with-gettext \
 --with-iconv-dir \
 --with-kerberos \
 --with-libdir=lib64 \
 --with-libxml-dir \
 --with-mysqli \
 --with-openssl \
 --with-pcre-regex \
 --with-pdo-mysql \
 --with-pdo-sqlite \
 --with-pear \
 --with-png-dir \
 --with-xmlrpc \
 --with-xsl \
 --with-zlib \
 --enable-fpm \
 --enable-bcmath \
 --enable-libxml \
 --enable-inline-optimization \
 --enable-gd-native-ttf \
 --enable-mbregex \
 --enable-mbstring \
 --enable-opcache \
 --enable-pcntl \
 --enable-shmop \
 --enable-soap \
 --enable-sockets \
 --enable-sysvsem \
 --enable-xml \
 --enable-zip
```

PHP错误列表： http://www.cnlvzi.com/index.php/Index/article/id/143


//mysql安装
rpm -Uvh  http://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm
yum -y install mysql-community-server
#vim /etc/my.cnf
如果修改端口：port=5328
添加一句:skip-grant-tables
重启mysql
进入mysql命令行修改密码：
update mysql.user set authentication_string=password('123456') where user='root' ;
SET PASSWORD = PASSWORD('123456');
flush privileges;

//添加自启动
http://www.jianshu.com/p/b5fa86d54685

//nginx
vi /lib/systemd/system/nginx.service
[Unit]
Description=nginx
After=network.target
[Service]
Type=forking
ExecStart=/usr/local/nginx/sbin/nginx
ExecReload=/usr/local/nginx/sbin/nginx -s reload
ExecStop=/usr/local/nginx/sbin/nginx -s quit
PrivateTmp=true
[Install]
WantedBy=multi-user.target


//php-fpm
vi /lib/systemd/system/php-fpm.service
[Unit]
Description=php-fpm
After=network.target
[Service]
Type=forking
ExecStart=/usr/local/php/sbin/php-fpm
ExecReload=killall php-fpm && /usr/local/php/sbin/php-fpm
ExecStop=killall php-fpm
PrivateTmp=true
[Install]
WantedBy=multi-user.target

[Unit]:服务的说明
Description:描述服务
After:描述服务类别
[Service]服务运行参数的设置
Type=forking是后台运行的形式
ExecStart为服务的具体运行命令
ExecReload为重启命令
ExecStop为停止命令
PrivateTmp=True表示给服务分配独立的临时空间
注意：[Service]的启动、重启、停止命令全部要求使用绝对路径
[Install]运行级别下服务安装的相关设置，可设置为多用户，即系统运行级别为3

先关闭nginx，php-fpm

systemctl enable nginx.service                #注意后面不能跟空格
systemctl enable php-fpm.service

shutdown -r now        #重启服务器
systemctl list-units --type=service           #查看运行的服务

systemctl start nginx.service              #启动nginx服务
systemctl enable nginx.service             #设置开机自启动
systemctl disable nginx.service            #停止开机自启动
systemctl status nginx.service             #查看服务当前状态
systemctl restart nginx.service　          #重新启动服务
systemctl list-units --type=service        #查看所有已启动的服务

