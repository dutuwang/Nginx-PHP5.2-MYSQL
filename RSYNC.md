首页明白道理，是将客户端的文件同步到服务端
#apt-get install rsync

服务端：
#cp /usr/share/doc/rsync/examples/rsyncd.conf /etc/
#vim /etc/rsyncd.conf

# GLOBAL OPTIONS                   #全局配置段
use chroot = no                    #是否chroot
strict modes = yes                 #严格模式
lock file = /var/lock/rsyncd       #程序锁文件存放位置
pid file = /var/run/rsyncd.pid     #pid文件位置
log file = /va/log/rsyncd.log      #日志文件位置

# MODULE OPTIONS                   #局部配置段
[backup]                           #模块名称
path = /data/backup                #同步文件存放目录
max connections=10                 #最多并发数
read only = no                     #开启读取
list = false                       #是否允许查看列表
#      exclude = lost+found/       #不同步的文件
#      exclude from =              #指定一个包含exclude模式定义的文件名
#      include =                   #类似exclude
#      include from =              #指定一个包含include模式定义的文件名
uid = root                         #以谁的身份运行rsync
gid = root                         #运行rsync的组
auth users = jevtse                #虚拟用户名（非linux用户），同步登录用
secrets file = /etc/rsyncd.secrets #虚拟用户的密码存放文件
hosts allow = 172.16.250.142       #白名单
hosts deny = *                     #黑名单
ignore errors = yes                #忽略I/O错误
ignore nonreadable = yes           #忽略没有访问文件权限的用户
transfer logging = no              #使用ftp格式独立记录日志
#       log format = %t: host %h (%a) %o %f (%l bytes). Total %b bytes.        #日志格式
timeout = 600                           #超时时间
refuse options = checksum dry -run      #客户端不能使用的命令
dont compress = *.gz *.tgz *.zip *.z *.rpm *.deb *.iso *.bz2 *.tbz        #不进行压缩传输的文件



客户端：
安装inotify-tools
inotifywait -rmq --timefmt '%y-%m-%d %H:%M' --format '%T %w %f %e' -e modify,delete,create,move,attrib /program/wwwroot/zgsx/ | while read line ; do rsync -avz --delete /program/wwwroot/zgsx/ rsyncuser@10.162.80.232::www --password-file=/etc/rsyncd.secrets ; done

#!/bin/bash
host=10.162.80.232 #被同步的服务器ip地址
src=/program/wwwroot/zgsx/ #需要同步的目录
des=www #被同步的一个模块名称
user=rsyncuser  
inotifywait -mrq --timefmt '%d/%m/%y %H:%M' --format '%T %w%f%e' -e modify,delete,create,attrib $src | while read files
do
rsync -aqzrtopg --delete --progress --password-file=/etc/rsyncd.secrets --exclude-from '/program/rsync.exclude.list' $src $user@$host::$des
echo "${files} was rsynced" >> /var/rsync.log 2>&1
done